#!/bin/sh

CONTAINERS="$@"
for c in $CONTAINERS
do
  echo ""
  echo "...$c down..."
  echo ""
  docker-compose -f "./ymlfiles/$c.yml" -p $c down
  done
