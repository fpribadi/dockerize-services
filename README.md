# Dockerize services

Run [Traefik v2](https://traefik.io/ "Traefik v2") + [Letsencrypt DNS Challenge](https://letsencrypt.org/docs/challenge-types/#dns-01-challengw "Letsencrypt DNS Challenge") + [Cloudflare](https://www.cloudflare.com/ "Cloudflare") in docker container with docker-compose

## Prerequisite

- Docker
- Cloudflare account
- domain name

## Installation

```
git clone https://gitlab.com/fpribadi/dockerize-services.git
cd dockerize-services
cp dot_env .env
htpasswd -Bc -C 16 internal/secrets/basic_auth_users.txt admin
mkdir internal/letsencrypt

```
modify .env, internal/secrets/cloudflare_api_key.txt and  internal/secrets/cloudflare_email.txt

```
docker-compose up -d
```
