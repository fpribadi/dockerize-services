#!/bin/sh

echo $(basename "$0")
CONTAINERS="$@"
for c in $CONTAINERS
do
  echo ""
  echo "...$c up..."
  echo ""
  docker-compose -f "./ymlfiles/$c.yml" -p $c up -d
  done"
